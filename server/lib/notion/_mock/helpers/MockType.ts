export type MockType =
  | "ListBlockChildrenResponse"
  | "GetPageResponse"
  | "GetDatabaseResponse"
  | "QueryDatabaseResponse"
  | "GetBlockResponse";
